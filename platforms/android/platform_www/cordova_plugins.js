cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-accountkit.plugin",
      "file": "plugins/cordova-plugin-accountkit/www/plugin.js",
      "pluginId": "cordova-plugin-accountkit",
      "clobbers": [
        "AccountKitPlugin"
      ],
      "runs": true
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-accountkit": "1.4.0",
    "cordova-plugin-whitelist": "1.3.4"
  };
});